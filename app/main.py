from flask import Flask, jsonify, make_response, request
from typing import Optional


app = Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/accelerators', methods=['GET'])
def get_accelerators():
    pass


@app.route('/catalog', methods=['GET'])
def get_catalog():
    pass


@app.route('/contacts', methods=['GET'])
def get_contacts():
    pass


@app.route('/contacts/<contact_id>', methods=['GET', 'DELETE', 'POST', 'PUT'])
def contacts_by_id(contact_id: Optional[int]):
    if request.method == 'GET':
        pass


@app.route('/investors', methods=['GET'])
def get_investors():
    pass


@app.route('/problems', methods=['GET'])
def get_problems():
    pass


@app.route('/projects', methods=['GET'])
def get_projects():
    pass


@app.route('/teams', methods=['GET'])
def get_teams():
    pass


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=8080)
